Categories:Navigation
License:Apache-2.0
Web Site:http://members.iinet.net.au/~ninelima/efis/
Source Code:https://github.com/ninelima/kwikEFIS
Issue Tracker:https://github.com/ninelima/kwikEFIS/issues
Changelog:https://github.com/ninelima/kwikEFIS/blob/HEAD/CHANGELOG.md
Bitcoin:1KKWRF25NwVgNdankr1vBphtkLbX766Ee1

Auto Name:Kwik EFIS
Summary:Electronic Flight Information System (EFIS)
Description:
Kwik EFIS is a Glass Cockpit application designed to work on most  Android
devices equipped with a GPS, gyroscope, accelerometer and a  CPU with reasonable
performance.

* Flight Director

Kwik EFIS has a fully functional flight director built in. It uses  the standard
V-Bar symbology common to modern flight directors.  The target waypoint and
altitude is set on-screen by means of the  spinner controls on the right top and
bottom of the screen.

* Demo Mode

There is also a "Demo Mode" available in the application. It is fairly
rudimentary and works like a crude flight simulator.  The heading and  altitude
can be changed by pitching and banking the device. The speed  runs up and down
automatically based on the pitch.

* [http://members.iinet.net.au/~ninelima/efis/gallery.html Screenshots]
.

Repo Type:git
Repo:https://github.com/ninelima/kwikEFIS

Build:2.2.0,3
    commit=42b6ea0131afd1cb2fa1ea4de2e1eb70258bb602
    subdir=app
    gradle=yes

Build:2.3,4
    commit=c21215e80c40fa191bc575b1e7615f10279763a0
    subdir=app
    gradle=yes

Build:2.3,6
    commit=e9eac9018e260ecc33737bc7c23f1c44e22e3e3c
    subdir=app
    gradle=yes

Build:2.4,7
    commit=9c984013ed2d808ab61326264286ec69d57b410d
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags ^PROD
Current Version:2.4
Current Version Code:7
